const express = require('express')
const router = express.Router()
const Todos = require('../Model/TodoSchema')
const uuid = require('uuid')
const mongoose = require('mongoose')

// GET - fetch all todos - all users /todos/todoItems
router.get('/alltodos', async (req, res) => {
    if (req.query.search == undefined) {
        const allTodos = await Todos.find()
        res.status(200).json(allTodos)
    } else {
        const allTodos = await Todos.find()
        const filterTodos = allTodos.filter(todo => {
            if (todo.todo.startsWith(req.query.search)) {
                return todo
            }
        })
        res.status(200).json(filterTodos)
    }
})

router.get('/alltodos/day', async (req, res) => {
    console.log(req.url)
    let filterTodos = []
    if (req.query.search === 'today') {
        const allTodos = await Todos.find()
        filterTodos = allTodos.filter(todo => {
            if (todo.createdAt.toString().split(' ')[2] == '09') {
                console.log(todo.createdAt.toString().split(' ')[2])
                return todo
            }
        })
    }
    if (req.query.search === 'yesterday') {
        const allTodos = await Todos.find()
        filterTodos = allTodos.filter(todo => {
            if (todo.createdAt.toString().split(' ')[2] == '08') {
                console.log(todo.createdAt.toString().split(' ')[2])
                return todo
            }
        })
    }
    if (req.query.search !== 'today' && req.query.search !== 'yesterday') {
        filterTodos = await Todos.find()
    }
    // filterTodos !== [] ? await Todos.find() : filterTodos
    res.status(200).json(filterTodos)
})

// get user todos - GET - /todos/todoItem
router.get('/:userId', async (req, res) => {
    const { userId } = req.params
    console.log(userId)
    if (!userId) {
        res.status(404).json({ message: 'User not found' })
    } else {
        const todosList = await Todos.find({ userId })
        res.status(200).json(todosList)
    }
})

// post/create todo - POST - /todos/todoItem(body)
router.post('/', async (req, res, next) => {
    const { userId, todo } = req.body
    const todoItem = await Todos.create({
        userId,
        todo
    })
    res.json(todoItem)
})

// PUT -  
router.put('/:todoId', (req, res, next) => {
    const todoId = req.params.todoId
    if (todoId) {
        const findTodo = Todos.find(todo => todo.id == todoId)
        const updatedTodo = {
            ...findTodo, ...req.body
        }
        res.json(updatedTodo)
    } else {
        next('Todo not found to update details')
    }
})

router.delete('/:todoId', async (req, res, next) => {
    const todoId = req.params.todoId
    await Todos.findByIdAndDelete(todoId)
    res.send('Deleted')
})

module.exports = router



