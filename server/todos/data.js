const todos = [
    {
        id: 1,
        item: 'First Todo',
        status: 'checked'
    }, {
        id: 2,
        item: 'Second Todo',
        status: 'unchecked'
    },
    {
        id: 1,
        item: 'Third Todo',
        status: 'checked'
    }
]

module.exports = todos