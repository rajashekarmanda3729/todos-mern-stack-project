const express = require('express')
const router = express.Router()
const todos = require('../Model/TodoSchema')
const jwtToken = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const Users = require('../Model/UserSchema')


const generateJwtToken = id => {
    return jwtToken.sign({ id }, process.env.SECRET_URI, { expiresIn: '30d' })
}

// - GET - getMe/user userdetails with user-id
router.get('/:userId', async (req, res) => {
    const userId = req.params.userId
    const findUser = await Users.findById(userId)
    const user = {
        ...findUser._doc,
        token: generateJwtToken(findUser._id)
    }
    res.status(404).json(user)
})

//POST - register user - /users - body
router.post('/register', async (req, res, next) => {

    const { user, email, password } = req.body

    if (!user || !email || !password) {
        res.status(400).json({ message: 'Please fill all user-fields' })
    }

    const foundUser = await Users.findOne({ email })

    if (foundUser) {
        res.status(400).json({ message: 'User already exist' })
    } else {
        // generating jwt token
        const salt = await bcrypt.genSalt(10)
        const hassedPassword = await bcrypt.hash(password, salt)

        console.log(hassedPassword)
        const userNew = await Users.create({
            user,
            email,
            password: hassedPassword,

        })
        const userData = {
            ...userNew._doc,
            token: generateJwtToken(userNew._id)
        }
        res.status(201).json(userData)
    }
})

router.post('/login', async (req, res) => {
    const { email, password } = req.body
    console.log(email, password)
    if (!email || !password) {
        res.status(400).json({ message: 'Please give user credentials' })
    }

    const findUser = await Users.find({ email })

    if (!findUser) {
        res.status(401).json({ message: 'Unauthorized user please register !' })
    }

    if (findUser[0] && (await bcrypt.compare(password, findUser[0].password))) {
        const userData = {
            ...findUser[0]._doc,
            token: generateJwtToken(findUser[0]._id)
        }
        res.status(200).json(userData)
    } else {
        res.send('wrong password/email')
    }
})

module.exports = router