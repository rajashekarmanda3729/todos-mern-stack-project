const express = require('express')
const app = express()
const cors = require('cors')
const dotenv = require('dotenv')
dotenv.config()
const PORT = 5000
const colors = require('colors')
const connectDb = require('./config/db')

const corsOptions = {
    origin: "*"
}

app.use(cors(corsOptions))

connectDb()

// to see res.body data on middleware
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

// endpoint - todos
app.use('/todos/todoItems', require('../server/todos/todoItems'))

//endpoint - user-todos
app.use('/users', require('../server/todos/usersTodos'))

// error-handle middleware
app.use((err, req, res, next) => {
    res.status(400).json({ message: err })
    // next()
})

app.listen(PORT, () => console.log(`Server runnig on PORT: ${PORT}`))

