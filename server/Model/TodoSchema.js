const mongoose = require('mongoose')

const TodoSchema = mongoose.Schema({
    userId: {
        type: String,
        required: [true, 'Please fill filed userId']
    },
    todo: {
        type: String,
        required: [true, 'Please fill filed todoname']
    },
},
    {
        timestamps: true
    }
)

module.exports = mongoose.model('Todos', TodoSchema)