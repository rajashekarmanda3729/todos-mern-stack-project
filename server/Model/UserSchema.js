const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    user: {
        type: String,
        required: [true, 'Please fill filed username']
    },
    email: {
        type: String,
        required: [true, 'Please fill filed email']
    },
    password: {
        type: String,
        required: [true, 'Please fill password']
    }
},
    {
        timestamps: true
    }
)

module.exports = mongoose.model('Users', userSchema)