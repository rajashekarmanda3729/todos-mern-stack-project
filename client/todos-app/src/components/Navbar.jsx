import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useNavigate } from 'react-router-dom'
import { logOut } from '../redux/todoSlice'

const Navbar = () => {

    const { loginUser, loginDetails } = useSelector(store => store.todos)

    const dispatch = useDispatch()
    const navigate = useNavigate()

    const authUser = localStorage.getItem('user')

    return (
        <div className='row'>
            <div className='col-12 pe-5 ps-5 p-3 d-flex justify-content-between bg-dark bg-opacity-50 text-light'>
                <Link to='/' className='col-9 d-flex text-light text-decoration-none'>
                    <h2 >{loginUser ? 'Dashboard-todos' : 'Home'}</h2>
                    {
                        loginUser && <h3 className='text-warning ms-4'>username:
                            {(loginUser && authUser != null) ? JSON.parse(authUser).user : loginDetails.user}
                            <button className='btn btn-danger' onClick={
                                () => {
                                    dispatch(logOut())
                                    navigate('/')
                                    localStorage.clear()
                                }}>LogOut</button>
                        </h3 >
                    }
                </Link>
                <div className='col-3 d-flex justify-content-between'>
                    {!loginUser && <Link to='/' className=' text-light text-decoration-none'>
                        <h2 >LogIn</h2>
                    </Link>}
                    {!loginUser && <Link to='/register' className=' text-light text-decoration-none'>
                        <h2 >SignUp</h2>
                    </Link>}
                    {
                        loginUser && <Link to='/alltodos' className=' text-light text-decoration-none'>
                            <h2 >All Todos</h2>
                        </Link>
                    }
                </div>
            </div>
        </div>
    )
}

export default Navbar