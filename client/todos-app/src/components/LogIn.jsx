import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useNavigate } from 'react-router-dom'
import { logIn, saveLoginDetails, userLoginAPI } from '../redux/todoSlice'
import Loading from './Loading'

const Login = () => {
    const { loginUser, isLoading } = useSelector(store => store.todos)

    const [loginData, setLoginData] = useState({
        email: '',
        password: '',
    })

    useEffect(() => {
        loginUser && navigate('/dashboard')
    }, [loginUser])


    const dispatch = useDispatch()
    const navigate = useNavigate()

    const handleChange = (event) => {
        setLoginData(prev => {
            return {
                ...prev,
                [event.target.name]: event.target.value
            }
        })
    }

    const onSubmitLogin = (event) => {
        event.preventDefault()
        const authUser = localStorage.getItem('user')
        if (authUser == null) {
            dispatch(saveLoginDetails(loginData))
            dispatch(userLoginAPI(loginData))
        } else {
            dispatch(logIn())
            navigate('/dashboard')
        }

    }

    if (isLoading) {
        return <Loading />
    }

    return (
        <div className='row'>
            <div className="col-12 p-5 d-flex flex-column justify-content-center align-items-center">
                <h1>Login</h1>
                <h4>Please Login to create a Todos (or) Create User
                    <Link to='/register'>
                        <button className='btn btn-warning'>SignUp</button>
                    </Link>
                </h4>
                <form onSubmit={onSubmitLogin} className='d-flex flex-column'>
                    <div className="d-flex flex-column">
                        <label htmlFor="email">Email</label>
                        <input type="email" id='email' name='email' value={loginData.email} onChange={handleChange} />
                    </div>
                    <div className="d-flex flex-column">
                        <label htmlFor="password">Password</label>
                        <input type="password" id='password' name='password' value={loginData.password} onChange={handleChange} />
                    </div>
                    <button type='submit' className="btn btn-primary mt-3">Login</button>
                </form>
            </div>
        </div>
    )
}

export default Login