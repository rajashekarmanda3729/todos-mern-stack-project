import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { registerUserAPI, saveRegisterDetails } from '../redux/todoSlice'
import Loading from './Loading'
// import { onStoreData, register, reset } from '../features/authSlice'

const SignUp = () => {
    const { loginUser, isLoading } = useSelector(store => store.todos)

    const [registerData, setRegisterData] = useState({
        username: '',
        email: '',
        password: '',
        confPassword: ''
    })

    const navigate = useNavigate()
    const dispatch = useDispatch()

    useEffect(() => {
        if (loginUser) {
            navigate('/dashboard')
        }
    }, [])

    // const { user, isLoading, isError, isSuccess, message } = useSelector(store => store.auth)

    const handleChange = (event) => {
        setRegisterData(prev => {
            // dispatch(onStoreData(prev))
            return {
                ...prev,
                [event.target.name]: event.target.value
            }
        })
    }

    const { username, email, password, confPassword } = registerData

    const onSubmitRegister = async (event) => {
        event.preventDefault()

        if (password !== confPassword) {
            throw new Error('confirm password doesn\' match')
        } else {
            if (!username || !email) {
                throw new Error('Please enter details for register')
            }
            const userData = {
                user: username, email, password
            }
            try {
                const requestOptions = {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(userData)
                };
                const response = await fetch(`http://localhost:5000/users/register`, requestOptions)
                const data = await response.json()
                console.log(data)
                if (data) {
                    navigate('/dashboard')
                }
            } catch (error) {
                console.log('Error in register user API')
            }
        }
    }

    if (isLoading) {
        return <Loading />
    }

    return (
        <div className='row'>
            <div className="col-12 p-5 d-flex flex-column justify-content-center align-items-center">
                <h1>Register</h1>
                <h4>Create a registered Id</h4>
                <form onSubmit={onSubmitRegister} className='d-flex flex-column'>
                    <div className="d-flex flex-column">
                        <label htmlFor="username">UserName</label>
                        <input type="text" id='username' name='username' value={registerData.username} onChange={handleChange} />
                    </div>
                    <div className="d-flex flex-column">
                        <label htmlFor="email">Email</label>
                        <input type="email" id='email' name='email' value={registerData.email} onChange={handleChange} />
                    </div>
                    <div className="d-flex flex-column">
                        <label htmlFor="password">Password</label>
                        <input type="password" id='password' name='password' value={registerData.password} onChange={handleChange} />
                    </div>
                    <div className="d-flex flex-column">
                        <label htmlFor="confPassword">Confirm Password</label>
                        <input type="password" id='confPassword' name='confPassword' value={registerData.confPassword} onChange={handleChange} />
                    </div>
                    <button type='submit' className="btn btn-primary mt-3">Click to register</button>
                </form>
            </div>
        </div>
    )
}

export default SignUp