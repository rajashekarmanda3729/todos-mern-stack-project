import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'

const Todos = () => {

    const [todoItems, setTodoItems] = useState([])
    const { loginDetails, loginUser } = useSelector(store => store.todos)

    useEffect(() => {
        const fetchTodos = async () => {
            const authUser = localStorage.getItem('user')
            let userId
            if (authUser != null && authUser != undefined) {
                userId = JSON.parse(authUser)._id
            } else {
                userId = loginDetails._id
            }
            const response = await fetch(`http://localhost:5000/todos/todoItems/${userId}`)
            const data = await response.json()
            setTodoItems(data)
            console.log(data)
        }
        fetchTodos()
    }, [])

    return (
        <div className='row'>
            <div className='d-flex flex-column justify-content-center align-items-center'>
                <h1>Todos</h1>
                <div className='col-12 d-flex flex-column justify-content-center align-items-center '>
                    {todoItems.map(todo => {
                        return <div className='d-flex col-2 flex-row' key={todo._id}>
                            <input type='checkbox' className='me-2' />
                            <label>{todo.todo}</label>
                        </div>
                    })}
                </div>
            </div>
        </div>
    )
}

export default Todos