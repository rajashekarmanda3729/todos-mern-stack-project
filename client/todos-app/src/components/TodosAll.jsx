import React, { useEffect, useState } from 'react'
import Loading from './Loading'
import { useDebouncedCallback } from 'use-debounce';

const TodosAll = () => {

    const [allTodos, setAllTodos] = useState([])
    const [load, setLoad] = useState(false)
    const [searchTodo, setSearchTodo] = useState('')
    const [byDate, setByDate] = useState('')

    useEffect(() => {
        async function getAllTOdos() {
            setLoad(true)
            const response = await fetch(`http://localhost:5000/todos/todoItems/alltodos`)
            const data = await response.json()
            setAllTodos(data)
            setLoad(false)
        }
        getAllTOdos()
    }, [])

    const debounce = useDebouncedCallback(async (value) => {
        setLoad(true)
        const response = await fetch(`http://localhost:5000/todos/todoItems/alltodos?search=${value}`)
        const data = await response.json()
        setAllTodos(data)
        setLoad(false)
    }, 2000)

    const onChangeDate = async (event) => {
        setByDate(event.target.value)
        const date = event.target.value
        try {
            setLoad(true)
            const response = await fetch(`http://localhost:5000/todos/todoItems/alltodos/day?search=${date}`)
            const data = await response.json()
            setAllTodos(data)
            setByDate(event.target.value)
            setLoad(false)
        } catch (error) {
            setLoad(false)
            console.log('fetching date by API error')
        }
    }

    return (
        <div className='row'>
            <div className='d-flex justify-content-evenly col-12 mt-2 p-2 w-100 mb-3 m-0 bg-success bg-opacity-25'>
                <div className='d-flex'>
                    <input className='' type="text" placeholder='todoItem' value={searchTodo}
                        onChange={(e) => {
                            debounce(e.target.value)
                            setSearchTodo(e.target.value)
                        }} />
                    <label className='fs-3 ms-2'>Todo Name</label>
                </div>
                <div className='col-6'>
                    <select value={byDate} onChange={onChangeDate}>
                        <option value='--Select Date--' name='select date'>--Select Date--</option>
                        <option value="today" name='today'>Today</option>
                        <option value="yesterday" name='yesterday'>Yesterday</option>
                    </select>
                </div>
            </div>
            <div className='col-12 d-flex flex-column justify-content-center align-items-center '>
                {allTodos.length > 0 && !load && allTodos.map(todo => {
                    return <div className='d-flex justify-content-between border m-1 shadow rounded-4 p-1 align-items-center col-6 flex-row' key={todo._id}>
                        <div className="d-flex justify-content-center align-items-center">
                            <input type='checkbox' className='me-2' />
                            <h6>{todo.todo}</h6>
                        </div>
                        <h5>{todo.createdAt}</h5>
                    </div>
                })}
                {
                    load && <Loading />
                }
            </div>
        </div>
    )
}

export default TodosAll