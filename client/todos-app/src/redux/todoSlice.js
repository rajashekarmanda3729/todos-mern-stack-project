import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

const authUser = localStorage.getItem('user')

const initialState = {
    loginDetails: authUser != null ? authUser : {},
    registerDetails: {},
    isLoading: false,
    loginUser: authUser != null ? true : false,
    todoDetails: {},
}

export const userLoginAPI = createAsyncThunk('todos/userLoginAPI',
    async (userData, asyncThunk) => {
        try {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(userData)
            };
            const response = await fetch(`http://localhost:5000/users/login`, requestOptions)
            const data = await response.json()
            // console.log(data)
            return data
        } catch (error) {
            asyncThunk.rejectWithValue('Something went wrong with login/API')
        }
    })

export const todosSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {
        saveLoginDetails: (state, { payload }) => {
            state.loginDetails = payload
        },
        saveRegisterDetails: (state, { payload }) => {
            state.registerDetails = payload
        },
        logOut: (state) => {
            state.loginDetails = {}
            state.loginUser = false
        },
        logIn: (state) => {
            state.loginUser = true
        }
    },
    extraReducers: {
        [userLoginAPI.pending]: (state, { payload }) => {
            state.isLoading = true
            state.loginUser = false
        },
        [userLoginAPI.fulfilled]: (state, { payload }) => {
            state.isLoading = false
            state.loginDetails = payload
            state.loginUser = true
            localStorage.setItem('user', JSON.stringify(payload))
        },
        [userLoginAPI.rejected]: (state, { payload }) => {
            state.isLoading = false
            state.loginUser = false
        }
    },
})

export const { saveLoginDetails, logOut, logIn, saveRegisterDetails } = todosSlice.actions

export default todosSlice.reducer