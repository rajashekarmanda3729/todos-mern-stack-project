import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Todos from './components/Todos'
import Navbar from './components/Navbar'
import LogIn from './components/LogIn'
import SignUp from './components/SignUp'
import TodosAll from './components/TodosAll'

function App() {

    return (
        <div className='container-fluid'>
            <BrowserRouter>
                <Navbar />
                <Routes>
                    <Route path='/' element={<LogIn />} />
                    <Route path='/dashboard' element={<Todos />} />
                    <Route path='/register' element={<SignUp />} />
                    <Route path='/alltodos' element={<TodosAll />} />
                </Routes>
            </BrowserRouter>

        </div>
    )
}
export default App